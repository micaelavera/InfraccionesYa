var Dibujador = function() {
    return {
        dibujarGruaEnMapa: dibujarGruaEnMapa,
        dibujarListaEstados: dibujarListaEstados,
		dibujarDeposito : dibujarDeposito
    }

    /******************************************************************************
     * Función para dibujar una grua en un mapa.
     */
    function dibujarGruaEnMapa(grua, map) {
		var marcador;
        console.log("Dibujando la grua: " + grua.id);
			
			var iconoGrua = L.icon({
                iconUrl: 'JS/leaflet/images/grua.ico',
            });
			
		 var info = "Grúa: " +grua.id +"<br>Estado: " +grua.estado.descripcion +"</br>";
			var actualIx = 0;
			setInterval(function(){
			 
			if (actualIx < grua.posiciones.length){
				console.log(grua.posiciones[actualIx].ubicacion.lat)
				var latitud = grua.posiciones[actualIx].ubicacion.lat;
				console.log(latitud);
                var longitud = grua.posiciones[actualIx].ubicacion.lon;
				
				if(marcador!=null){
				   map.removeLayer(marcador);
		 }
				
		marcador = L.marker(L.latLng(latitud, longitud),{icon: iconoGrua})
			.bindPopup(info).addTo(map);
				
			actualIx = actualIx + 1;
			
		 }}
			 ,500);	 
	}
	
    /******************************************************************************
     * Función para listar los estados en la página.
     */
    function dibujarListaEstados(estados, nodeId) {        
		estados.forEach(function(estado) {
            var li = $('<li>');
            li.append(estado.descripcion);
            $("#"+nodeId).append(li);
        });
    }
	
	 /******************************************************************************
     * Función para dibujar el depósito
     */
	function dibujarDeposito(latitud,longitud,deposito,map){
		var info = "Depósito: " + deposito.nombre + "<br>Dirección: " + deposito.direccion + "<br>Horarios: " + deposito.horarios + "<br>Teléfono: " + deposito.telefono + "</br>";
	
		var iconoDeposito= L.icon({
                iconUrl: 'JS/leaflet/images/deposito.ico',
            });
			
		var circle = L.circle((L.latLng(latitud,longitud)), {
			color: '#0000AA',
			fillColor: '#0000CC',
			fillOpacity: 0.2,
			radius: 300
		}).addTo(map);

		var deposito = L.marker(L.latLng(latitud, longitud),{icon: iconoDeposito})
		.bindPopup(info).addTo(map);
	}
}