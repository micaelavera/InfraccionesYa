
var bootstrap = function() {
	var urlGruas = '/gruas/';
	var urlEstados = '/estadosGruas/';
	var urlPosiciones = '/posiciones/';
	var urlApi = Config.urlApi;
	
    var map = createMap('mapidGrua');
    var dibujador = new Dibujador();
	
    var requestGrua = function(grua_id) {
        return $.ajax(urlApi + urlGruas + grua_id);
    }
    var requestEstado = function(estado_id) {
        return $.ajax(urlApi + urlEstados + estado_id);
    }
	
	var requestPosiciones = function(grua_id){
		return $.ajax(urlApi + urlGruas + grua_id + urlPosiciones); 
	}
	
    var responseExtract = function(attr, response) {
        console.log(response);
        return response[attr]
    }
	
    var extractGrua = function(response) {
        return responseExtract('grua', response);	
    }

    var extractEstado = function(response) {
        return responseExtract('estado', response);
    }
	
    var dibujarGrua= function(grua) {
		dibujador.dibujarGruaEnMapa(grua, map);
    }
	
    var resolverEstadoGrua = function(grua) {
        // pedimos el estado con el estado_id, y retornamos la grua completa
        return requestEstado(grua.estado_id)
               .then(function(response){
                    grua.estado = response.estado;
                    delete grua.estado_id;                    
                    return grua;        
                })
    }

	 var resolverPosiciones = function(grua) {
        return requestPosiciones(grua.id)
               .then(function(response){
                    grua.posiciones = response.posiciones;       
					//console.log(grua.posiciones);					
                    return grua;
                })
    }
    // comenzamos la ejecución:
	var url = urlApi + urlGruas;
    $.ajax({
        url: url,
        success: function(respuesta) { //traemos todas las gruas
        respuesta.gruas.forEach(function(grua){
			requestGrua(grua.id)         // pedimos la grua al servidor
			.then(extractGrua)          // extraemos la grua de la respuesta del servidor
			.then(resolverEstadoGrua)   // resolvemos el estado        
			.then(resolverPosiciones)   //resolvemos las posiciones
			.then(dibujarGrua)          // dibujamos la grua con su estado
			.done(function() {
            console.log("Fin.");
			});
		 })
	   }
	});
};

$(bootstrap);