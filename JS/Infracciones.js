﻿function consultarInfracciones(patente) {
    $('#tablita').empty();
    $('#mapid').remove();
    $('txtAcarreo').empty();
    var div = "<div id='mapid'></div>";
    var btn = document.createElement("div");
    btn.innerHTML = div;
    document.getElementById('contenido').appendChild(btn);
    document.getElementById('txtAcarreo').innerHTML = "";
	
	var urlApi = Config.urlApi;
	var dibujador = new Dibujador();
    var url = urlApi + "/" + patente + "/infracciones";
	
    $.ajax({
        url: url,
        success: function (respuesta) {
            /*la manera de traer el dato del JSON*/
            respuesta.infracciones.forEach(function (infraccion) {
                console.log(infraccion.id);
                $.ajax({
                    url: urlApi + "/tiposinfraccion/" + infraccion.tipoInfraccion,
                    success: function (tipoInfraccion) {

                        infraccion.tipo = tipoInfraccion;
                        var txtTipoInfraccion = infraccion.tipo.tipo.descripcion;

                        if (infraccion.existeAcarreo == true) {
                            $.ajax({
                                url: urlApi + "/" + infraccion.patente + "/acarreos/" + infraccion.id,
                                success: function (acarreo) {

                                    var deposito = acarreo.acarreo.deposito;

                                    generarDescripcion(patente, deposito.nombre, deposito.direccion, deposito.horarios, deposito.telefono);

                                    generarTabla(patente, txtTipoInfraccion, infraccion.fechaHoraRegistro, infraccion.montoAPagar,
                                        infraccion.existeAcarreo);
										var map = createMap('mapid'); //crea el mapa con el acarreo siempre cuando acarreo sea true
										dibujador.dibujarDeposito(deposito.ubicacion.lat, deposito.ubicacion.lon, deposito,map);
                                }
							
                            })
                        }
                        generarTabla(patente, txtTipoInfraccion, infraccion.fechaHoraRegistro, infraccion.montoAPagar, infraccion.existeAcarreo);
                    }

                })

            })
        },

        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });

}

function generarDescripcion(patente, nombre, direccion, horario, telefono) {
    var contenido = "Su vehículo con la patente: " + patente + " fue acarreado por " + nombre + "<br>La dirección del depósito donde se encuentra es " + direccion + "</br>" + "<br> Nuestros horarios de atención son de " + horario + ". Ante cualquier consulta comuníquese al " + telefono + ".</br>";
    document.getElementById('txtAcarreo').innerHTML = contenido;
}

function generarTabla(Patente, txtInfraccion, horario, monto, deposito) {
    horario = convert(horario);
    deposito = modificarDeposito(deposito);

    if (!contiene(Patente, txtInfraccion, horario, monto, deposito)) {

        var fila = "<tr><td>" + Patente + "</td><td>" + txtInfraccion + "</td><td>" + horario + "</td><td>" + monto + "</td><td>" + deposito + "</td></tr>";
        var btn = document.createElement("TR");
        btn.innerHTML = fila;
        document.getElementById("tablita").appendChild(btn);
    }
}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
		hours = ("0" + date.getUTCHours()).slice(-2);
		minutes = ("0" + date.getMinutes()).slice(-2);
		return [day, mnth, date.getFullYear() + " " + hours + ":" + minutes + "hs"].join("-");
}

function contiene(patente, infraccion, horario, monto, deposito) {
    var table = document.getElementById('tablita');
    var cantFilas = table.rows.length;
    for (var fila = 0; fila < cantFilas; fila++) {
        if ((table.rows[fila].cells[0].innerHTML) == patente &&
            (table.rows[fila].cells[1].innerHTML) == infraccion &&
            (table.rows[fila].cells[2].innerHTML) == horario &&
            (table.rows[fila].cells[3].innerHTML) == monto &&
            (table.rows[fila].cells[4].innerHTML) == deposito) {

            return true;
        }
    }
    return false;
}

function modificarDeposito(deposito) {
    if (deposito == true) {
        return "Si";
    } else {
        return "No";
    }
}